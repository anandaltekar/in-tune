
import UIKit
import FBSDKLoginKit

class showListVC: UIViewController, UITableViewDataSource, UITableViewDelegate,  _IFaceCallTarget
{
    @IBOutlet weak var showListTV: UITableView!
    @IBOutlet var verifyPopup: UIView!
    @IBOutlet var verifyPopCont: UIView!
    @IBOutlet var verifyTextField: UITextField!
    @IBOutlet var verifyBtn: UIButton!
    @IBOutlet var tableVwContainer: UIView!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userTagLine: UILabel!
    
    var soapObject:DisplayListOfConcerts!
    var codeSoapObject:VerifyConcertCode!
    var notifSoapObject:GetUpdatedNotifications!
    var flagToLoadTut:Int!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        showListTV.layer.cornerRadius = 5.0
        showListTV.delegate = self
        showListTV.dataSource = self
        
        let params:[String] = [publicSuidSession]
        
        notifSoapObject = GetUpdatedNotifications()
        notifSoapObject.pageTarget = self
        notifSoapObject.request(params)
        let connector2:WSSoapConnector = WSSoapConnector()
        connector2.callAPI(notifSoapObject)
        
        soapObject = DisplayListOfConcerts()
        soapObject.pageTarget = self
        soapObject.request(params)
        let connector:WSSoapConnector = WSSoapConnector()
        connector.callAPI(soapObject)
        
        verifyPopCont.layer.cornerRadius = 7.0
        verifyBtn.layer.cornerRadius = 5.0
        userName.text = fbFirstName + " " + fbLastName
        hidePopup()
        Utility.downloadFBPP(fbID, imgView: self.userImage, isLargePic: true)
        //Utility.downloadProfilePicture(fbProfilePictureURL, imgView: self.userImage)
        self.userImage.layer.cornerRadius = self.userImage.frame.height/2
        self.userImage.clipsToBounds = true
        let tapExit: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hidePopup")
        let tapDismissKeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        verifyPopup.addGestureRecognizer(tapExit)
        verifyPopCont.addGestureRecognizer(tapDismissKeyboard)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "intune_background.jpg")!)
        
        Utility.pageGredient(self.view)
        
//        let rightBtn : UIBarButtonItem = UIBarButtonItem(title: "Setting", style: UIBarButtonItemStyle.Plain, target: self, action: "SettingAction")
        let rightBtn = UIButton()
        rightBtn.setImage(UIImage(named: "setting.png"), forState: UIControlState.Normal)
        rightBtn.targetForAction("SettingAction", withSender: self)
        rightBtn .addTarget(self, action: "SettingAction", forControlEvents: UIControlEvents.TouchUpInside)
        rightBtn.frame = CGRectMake(0, 0, 25, 25)
        rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let barButton = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Reloading...")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.showListTV.addSubview(refreshControl)
        
        let logo = UIImage(named: "navBarLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.navigationItem.hidesBackButton = true
        
    }
    override func viewWillAppear(animated: Bool) {
        if navigateTo == Const.NOTIF_CHAT_MSG && soapObject.showList.count>0 {
//            selectedConcert = Utility.intToSuid(soapObject.showList[0].id)
//            if soapObject.showList[0].isUserVerified == Const.USER_VERIFIED
//            {
//                self.performSegueWithIdentifier("codeToMainScreen", sender: self)
//            }
//            else
//            {
//                hidePopup(false)
//                soapObject.showList[0].isUserVerified = Const.USER_VERIFIED
//            }
        }
    }
    func SettingAction(){
        self.performSegueWithIdentifier("settingPage", sender: self)
//        if SRTimer.timerStatus == true
//        {
//            //display alert
//        }
//        else
//        {
//            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to log out?", preferredStyle: .ActionSheet)
//            let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
//                let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//                fbLoginManager.logOut()
//                Utility.resetPublicVars()
//                self.navigationController?.popToRootViewControllerAnimated(false)
//            })
//            let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
//            alert.addAction(confirmAction)
//            alert.addAction(CancelAction)
//            self.presentViewController(alert, animated: true, completion: nil)
//        }
        
    }
    func refresh(){
        soapObject = DisplayListOfConcerts()
        soapObject.pageTarget = self
        let params:[String] = [publicSuidSession]
        soapObject.request(params)
        let connector:WSSoapConnector = WSSoapConnector()
        connector.callAPI(soapObject)
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    func hidePopup(val:Bool){
        verifyPopup.hidden = val
    }
    func hidePopup(){
        hidePopup(true)
        dismissKeyboard()
    }
    @IBAction func verifyBtnAction(sender: AnyObject){
        dismissKeyboard()
        if verifyTextField.text != ""
        {
            let params:[String] = [publicSuidSession, selectedConcert ,verifyTextField.text!]
            codeSoapObject = VerifyConcertCode()
            codeSoapObject.pageTarget = self
            codeSoapObject.request(params)
            let soapConnector:WSSoapConnector = WSSoapConnector()
            soapConnector.callAPI(codeSoapObject)
        }
    }
    func verifyShowUpdate(){
        let selectedID:Int = Utility.suidToInt(selectedConcert)
        for var i:Int = 0; i<soapObject.showList.count; i++ {
            if soapObject.showList[i].id == selectedID {
                soapObject.showList[i].isUserVerified = Const.USER_VERIFIED
                break
            }
        }
        self.showListTV.reloadData()
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return soapObject.showList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("showListCell") as! showListCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.loadData(soapObject.showList[indexPath.row])
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if userDefaults.valueForKey("flagToLoadTut") == nil && soapObject.showList[indexPath.row].isUserVerified == Const.USER_VERIFIED
        {
            flagToLoadTut = Const.USER_VERIFIED
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        selectedConcert = Utility.intToSuid(soapObject.showList[indexPath.row].id)
        if soapObject.showList[indexPath.row].isUserVerified == Const.USER_VERIFIED
        {
            self.performSegueWithIdentifier("codeToMainScreen", sender: self)
        }
        else
        {
            hidePopup(false)
            //soapObject.showList[indexPath.row].isUserVerified = Const.USER_VERIFIED
        }
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell!.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
        let delay = 1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) {
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            cell!.backgroundColor = UIColor.clearColor()
        }
        
    }
    func serviceResponse(target: _IFaceAPI){
        hidePopup()
        if target.methodName == "VerifyConcertCode"
        {
            if codeSoapObject.respCode == 0
            {
                if userDefaults.valueForKey("flagToLoadTut") == nil
                {
                    flagToLoadTut = 1
                    userDefaults.setValue(flagToLoadTut, forKey: "flagToLoadTut")
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("firstTutVC") as! firstTutVC
                    vc.flagForSelectingPageImages = 1
                    self.presentViewController(vc, animated: false, completion: nil)
                }
                verifyShowUpdate()
                self.performSegueWithIdentifier("codeToMainScreen", sender: self)
            }
            else
            {
                let alert = UIAlertController(title: "Oops!", message: "Are you really at (Venuename)?? please enter the code again!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        else if target.methodName == "DisplayListOfConcerts"
        {
            self.showListTV.reloadData()
            self.refreshControl.endRefreshing()
        }
        else if target.methodName == "GetUpdatedNotifications"
        {
            //var matchNotifList:[structNotification] = DBChats.getUnreadNotifs(Const.NOTIF_MATCH_FOUND)
            //            if matchNotifList.count>0 {
            //                let navC = self.navigationController
            //                let storyboard = UIStoryboard(name: "in-tune-Main", bundle: nil)
            //                let destinationController = storyboard.instantiateViewControllerWithIdentifier("matchVC") as? matchVC
            //                destinationController?.structGroup = Utility.notificationToGroup(matchNotifList[0])
            //                destinationController?.notificationID = matchNotifList[0].id
            //                navC!.presentViewController(destinationController!, animated: false, completion: nil)
            //
            //            }
        }
    }
    func dismissKeyboard(){
        view.endEditing(true)
    }
}

