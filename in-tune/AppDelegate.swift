    
import UIKit
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, _IFaceCallTarget
{
    var currentDeviceToken:String = ""
    var chatRoom:chatRoomVC!
    var mainTabBar:mainTabBarController!
    var window: UIWindow?
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings);
        UIApplication.sharedApplication().registerForRemoteNotifications();
//        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
//        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
//        if let notificationPayload = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary
//        {
//            print("\(notificationPayload)")
//            print("\(launchOptions)")
//            _ = notificationPayload
//        }
        
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
        pageControl.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        
        // Override point for customization after application launch.
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        print("devicetoken is: ", deviceToken)
//        let navC = window?.rootViewController as! UINavigationController
//        let storyboard = UIStoryboard(name: "in-tune-Main", bundle: nil)
//        let alert = UIAlertController(title: "Logout", message: "\(deviceToken)", preferredStyle: .ActionSheet)
//        
//        let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
//            navC.navigationController?.popToRootViewControllerAnimated(false)
//            
//        })
//        let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
//        alert.addAction(confirmAction)
//        alert.addAction(CancelAction)
//        navC.presentViewController(alert, animated: true, completion: nil)
        
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        currentDeviceToken = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        if let aps = userInfo["aps"] as? NSDictionary
        {
            if let message:NSString = aps["category"] as? NSString
            {
                let str:String = message as String //[Group ID]#$#[User-Name]#$#[message]#$#[message-ID]#$#[user-ID]#$#[Host/Guest]#$#[Notification-ID]
                //str = "200#$#Shambhavi Thatte#$#Test Message#$#234#$#48#$#0#$#346"
                //str = "2#$#High Spirits#$#2#*#Anand Altekar#*#10155768532340063$#$3#*#Akshay Shinde#*#940832245959355$#$4#*#Akshay Shinde#*#940832245958515#$#444"
                //str = "2#$#High Spirits#$#2#*#Anand Altekar#*#10155768532340063#$#444"
                var msgArr:[String] = str.componentsSeparatedByString(Const.NOTIF_JOIN_LVL1)
                
                if let notifType:Int = aps["content-available"] as? Int
                {
                    var notif:structNotification = structNotification()
                    if notifType == Const.NOTIF_CHAT_MSG
                    {
                        //handling notification when app is active to display local notification
                        let state: UIApplicationState = application.applicationState
                        if state == UIApplicationState.Active
                        {
                            print("You've got a new message!", terminator: "")
                            let notification: CWStatusBarNotification = CWStatusBarNotification()
                            notification.notificationStyle = CWNotificationStyle.NavigationBarNotification
                            notification.notificationAnimationInStyle = CWNotificationAnimationStyle.Top
                            notification.notificationAnimationOutStyle = CWNotificationAnimationStyle.Right
                            notification.notificationLabelTextColor = UIColor.whiteColor()
                            notification.notificationLabelFont = UIFont(name: "Aerial", size: 20)
                            notification.notificationLabelBackgroundColor = UIColor(netHex: 0x71985e)
                            notification.displayNotificationWithMessage("You've got a new message!", forDuration: 3.0)
                            
                        }
                        notif.id = Int(msgArr[msgArr.count-1])!
                        notif.message = msgArr[2]
                        notif.idUser = Int(msgArr[4])!
                        notif.groupId = Int(msgArr[0])!
                        notif.messageId = Int(msgArr[3])!
                        notif.type = notifType
                        notif.other = msgArr[1]
                        notif.messageType = Int(msgArr[5])! + 1 // Converting to host or guest message
                        DBChats.addNotifications([notif])
                        if application.applicationState == UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background
                        {
                            navigateTo = Const.NOTIF_CHAT_MSG
//                            var navController:UINavigationController = self.window?.rootViewController as! UINavigationController
//                            if navController.viewControllers != nil {
//                                let storyboard = UIStoryboard(name: "in-tune-Main", bundle: nil)
//                                let vc = storyboard.instantiateViewControllerWithIdentifier("chatRoomVC") as! chatRoomVC
//                                navController.visibleViewController.navigationController?.pushViewController(vc, animated: false)
                                //navController.performSegueWithIdentifier("chatRoom", sender: nil)
                                //navController.pushViewController(vc, animated: false)
                                //}
//                            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "in-tune-Main", bundle: nil)
//                            let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewControllerWithIdentifier("chatRoomVC") as! UIViewController
//                            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
//                            self.window?.rootViewController = initialViewControlleripad
//                            self.window?.makeKeyAndVisible()
                        }
                    }

                    else if notifType == Const.NOTIF_MATCH_FOUND
                    {
                        notif.id = Int(msgArr[msgArr.count-1])!
                        notif.message = msgArr[1]
                        notif.groupId = Int(msgArr[0])!
                        notif.type = notifType
                        notif.other = msgArr[2]
                        DBChats.addNotifications([notif])
                        
                        var strGroup:structChatMsgGroup = structChatMsgGroup()
                        strGroup.id = Int(msgArr[0])!
                        strGroup.name = msgArr[1]
                        let userInfoArr:[String] = msgArr[2].componentsSeparatedByString(Const.NOTIF_JOIN_LVL2)
                        for item in userInfoArr {
                            var userDetailsArr:[String] = item.componentsSeparatedByString(Const.NOTIF_JOIN_LVL3)
                            if Utility.suidToInt(publicSuidUser) != Int(userDetailsArr[0])! {
                                var userStrct:structChatMsgUser = structChatMsgUser()
                                userStrct.id = Int(userDetailsArr[0])!
                                userStrct.name = userDetailsArr[1]
                                userStrct.fbProfUrl = userDetailsArr[2]
                                strGroup.userList.append(userStrct)
                            }
                        }
                        let navC = window?.rootViewController as! UINavigationController
                        let storyboard = UIStoryboard(name: "in-tune-Main", bundle: nil)
                        let destinationController = storyboard.instantiateViewControllerWithIdentifier("matchVC") as? matchVC
                        destinationController?.structGroup = strGroup
                        destinationController?.notificationID = Int(msgArr[msgArr.count-1])!
                        navC.presentViewController(destinationController!, animated: false, completion: nil)
                    }
                    else if notifType == Const.NOTIF_PLAY_NEXT_SONG
                    {
                        //handling notification when app is active to display local notification
                        let state: UIApplicationState = application.applicationState
                        if state == UIApplicationState.Active
                        {
                            print("song playing next received", terminator: "")
                            let notification: CWStatusBarNotification = CWStatusBarNotification()
                            notification.notificationStyle = CWNotificationStyle.NavigationBarNotification
                            notification.notificationAnimationInStyle = CWNotificationAnimationStyle.Top
                            notification.notificationAnimationOutStyle = CWNotificationAnimationStyle.Right
                            notification.notificationLabelTextColor = UIColor.whiteColor()
                            notification.notificationLabelFont = UIFont(name: "Aerial", size: 20)
                            notification.notificationLabelBackgroundColor = UIColor(netHex: 0x71985e)
                            notification.displayNotificationWithMessage("Your song is playing next!", forDuration: 3.0)
                        }
                    }
                    else if notifType == Const.NOTIF_APP_UPDATE
                    {
                        //
                    }
                    else if notifType == Const.NOTIF_USER_ACCEPTED
                    {
                        //handling notification when app is active to display local notification
                        let state: UIApplicationState = application.applicationState
                        if state == UIApplicationState.Active
                        {
                            print("New user added to your group!", terminator: "")
                            let notification: CWStatusBarNotification = CWStatusBarNotification()
                            notification.notificationStyle = CWNotificationStyle.NavigationBarNotification
                            notification.notificationAnimationInStyle = CWNotificationAnimationStyle.Top
                            notification.notificationAnimationOutStyle = CWNotificationAnimationStyle.Right
                            notification.notificationLabelTextColor = UIColor.whiteColor()
                            notification.notificationLabelFont = UIFont(name: "Aerial", size: 20)
                            notification.notificationLabelBackgroundColor = UIColor(netHex: 0x71985e)
                            notification.displayNotificationWithMessage("New user added to your group!", forDuration: 3.0)
                            
                        }
                        notif.id = Int(msgArr[msgArr.count-1])!
                        notif.message = msgArr[1]
                        notif.groupId = Int(msgArr[0])!
                        notif.type = notifType
                        notif.other = msgArr[2]
                        DBChats.addNotifications([notif])
                    }
                    else if notifType == Const.NOTIF_ALREADY_MATCH {
                        notif.id = Int(msgArr[msgArr.count-1])!
                        notif.message = msgArr[2]
                        notif.groupId = Int(msgArr[0])!
                        notif.type = notifType
                        notif.messageId = Int(msgArr[3])!
                        notif.messageType = Int(msgArr[4])!
                        notif.other = msgArr[1]
                        DBChats.addNotifications([notif])
                    }
                }
                //Don't delete this code - updating unread notifications to server
                let soapObject:UpdateNotification = UpdateNotification()
                let params:[String] = [publicSuidSession, msgArr[msgArr.count-1]]
                soapObject.request(params)
                let connector:WSSoapConnector = WSSoapConnector()
                connector.callAPI(soapObject)
            }
        }
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification)
    {
        //
    }
    
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication)
    {
        sleepTime = Int(NSDate.timeIntervalSinceReferenceDate())
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        wakeTime = Int(NSDate.timeIntervalSinceReferenceDate())
    }
    
    func applicationDidBecomeActive(application: UIApplication)
    {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        //
    }
}