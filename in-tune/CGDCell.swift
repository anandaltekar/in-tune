//
//  CGDCell.swift
//  in-tune
//
//  Created by Anand Altekar on 02/09/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class CGDCell: UITableViewCell
{
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet var cellContainer: UIView!
    
    var userInfo: structChatMsgUser!
    
    func bindData()
    {
        Utility.downloadFBPP(userInfo.fbProfUrl, imgView: userImage, isLargePic: false)
        userName.text = userInfo.name
    }
    override func awakeFromNib()
    {
        super.awakeFromNib()
        cellContainer.layer.cornerRadius = 4.0
        //userImage.layer.cornerRadius = userImage.bounds.size.height/5
        userImage.layer.cornerRadius = 4.0
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
