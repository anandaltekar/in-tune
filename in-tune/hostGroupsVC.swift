//
//  hostGroupsVC.swift
//  in-tune
//
//  Created by Anand Altekar on 22/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostGroupsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, _IFaceCallTarget
{
    var tappedIndex:Int = 0
    var soapObject:GetUserCommuniqueboxes!
    @IBOutlet var tableView: UITableView!
    var refreshControl:UIRefreshControl!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Utility.pageGredient(self.view)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Reloading...")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        loadGroups()
    }
    
    func refresh()
    {
        loadGroups()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.soapObject.chatGroupList.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("hostGroupsCell", forIndexPath: indexPath) as! hostGroupsCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.bindData(self.soapObject.chatGroupList[indexPath.row])

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        tappedIndex = indexPath.row
        performSegueWithIdentifier("showHostChatVC", sender: self)
    }
    func loadGroups()
    {
        Utility.pageGredient(self.view)
        soapObject = GetUserCommuniqueboxes();
        soapObject.pageTarget = self
        let params:[String] = [publicSuidSession]
        soapObject.request(params)
        let serviceConnector:WSSoapConnector = WSSoapConnector()
        serviceConnector.callAPI(soapObject)
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if soapObject.respCode == 0
        {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
        else
        {
            print("response error")
            //display alert
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "showHostChatVC"
        {
            let destViewControler : hostChatVC = segue.destinationViewController as! hostChatVC
            let id:Int = soapObject.chatGroupList[self.tappedIndex].id
            destViewControler.idGroup = id
        }
    }
}
