//
//  hostChatDetailsVC.swift
//  in-tune 
//
//  Created by Anand Altekar on 11/09/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostChatDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var userInfo: [structChatMsgUser]!
    var groupID: Int!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        userInfo = DBChats.getGroupUsers(groupID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }
    
    func DetailsAction()
    {
        performSegueWithIdentifier("showHostChatDetails", sender: self)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return userInfo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("CGDItem") as! CGDCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.userInfo = userInfo[indexPath.row]
        cell.bindData()
        
        return cell
    }
}
