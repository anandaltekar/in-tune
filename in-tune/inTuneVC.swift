//
//  inTuneVC.swift
//  in-tune
//
//  Created by Anand Altekar on 31/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class inTuneVC: UIViewController, UITableViewDataSource, UITableViewDelegate, _IFaceCallTarget
{
    @IBOutlet weak var tableView: UITableView!
    //var groupDetails:[chatGroupDetails] = Array<chatGroupDetails>()
    var soapObject:GetUserCommuniqueboxes!
    var soapObjectDeleteChat: RemoveUserFromCommuniqueBox = RemoveUserFromCommuniqueBox()
    var tappedIndex:Int = 0
    var chatGroupList:[structChatMsgGroup] = Array<structChatMsgGroup>()
    var refreshControl:UIRefreshControl!
    
    @IBOutlet var pageStatusLbl: UILabel!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        callToGetUserCommuniqueboxes()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Reloading...")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    func backBtnAction(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func refresh(){
        callToGetUserCommuniqueboxes()
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(animated: Bool){
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.hidden = false
        if publicChatUpdates {
            loadData()
            publicChatUpdates = false
        }
    }
    //MARK:- Tableview Datasource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.chatGroupList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("groupsCell") as! MessagesCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.bindData(self.chatGroupList[indexPath.row])
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath) as? MessagesCell
        cell?.setUnsetBadge(false)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        tappedIndex = indexPath.row
        self.performSegueWithIdentifier("chatRoom", sender: nil)
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        //
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?{
        tappedIndex = indexPath.row
        let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete",
            handler: { (action: UITableViewRowAction, indexPath: NSIndexPath) in
                print("Delete Pressed")
                self.confirmDelete()
            }
        );
        deleteAction.backgroundColor = UIColor.redColor()
        return [deleteAction]
    }
    func confirmDelete(){
        let alert = UIAlertController(title: "Delete Chat", message: "Do you really want to delete this chat?", preferredStyle: .ActionSheet)
        
        let confirmAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
            let groupID:Int = self.chatGroupList[self.tappedIndex].id
            DBChats.updateGroupStatus(groupID, value: Const.INACTIVE)
            self.soapObjectDeleteChat.request([publicSuidSession, Utility.intToSuid(groupID)])
            let soapConnector:WSSoapConnector = WSSoapConnector()
            soapConnector.callAPI(self.soapObjectDeleteChat)
            self.tableView.editing = false
            self.loadData()
        })
        
        let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil )
        alert.addAction(confirmAction)
        alert.addAction(CancelAction)
        
        self.presentViewController(alert, animated: true, completion:nil)
    }
    //MARK:- Service Responses
    func callToGetUserCommuniqueboxes(){
        loadData()
        self.pageStatusLbl.hidden = false
        self.pageStatusLbl.text = "Checking for Conversations ..."
        soapObject = GetUserCommuniqueboxes();
        soapObject.pageTarget = self
        let params:[String] = [publicSuidSession]
        soapObject.request(params)
        let serviceConnector:WSSoapConnector = WSSoapConnector()
        serviceConnector.callAPI(soapObject)
    }
    func serviceResponse(target: _IFaceAPI){
        loadData()
         self.refreshControl.endRefreshing()
    }
    func loadData(){
        print("$$@@##@@$$-IntuneVC-loadDataCalled")
        chatGroupList = DBChats.getGroups()
        if chatGroupList.count <= 0 {
            self.pageStatusLbl.hidden = false
            self.pageStatusLbl.text = "No Conversations found!"
        }
        else {
            self.pageStatusLbl.hidden = true
        }
        self.tableView.reloadData()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        if segue.identifier == "chatRoom"
        {
            let destViewControler : chatRoomVC = segue.destinationViewController as! chatRoomVC
            let id:Int = self.chatGroupList[self.tappedIndex].id
            destViewControler.idGroup = id
            destViewControler.nameGroup = Utility.userNamesExceptYou(self.chatGroupList[self.tappedIndex].userList)
        }
    }
    func checkUpdates(data:[[Int:Int]]){
        if data.count>0
        {
            chatGroupList = DBChats.getGroups()
            self.tableView.reloadData()
        }
    }
}
