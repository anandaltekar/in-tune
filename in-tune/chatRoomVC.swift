
import UIKit

class chatRoomVC: UIViewController, UITableViewDataSource, UITableViewDelegate, _IFaceCallTarget {
    
    @IBOutlet var tableVw: UITableView!
    @IBOutlet var frameView: UIView!
    @IBOutlet var sendChatBtn: UIButton!
    @IBOutlet var chatInputField: UITextField!
    @IBOutlet var bottomMargin: NSLayoutConstraint!
    
    @IBOutlet var inputContainer: UIView!
    
    var soapObject:GetCommuniqueBoxNotes!
    var soapObjSendMsg:SendNoteToCommuniqueBox!
    var idGroup:Int = 0
    var nameGroup:String = ""
    var chatList:[structChatMsg] = Array<structChatMsg>()
    var marginVal:CGFloat = 0
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad(){
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        tableVw.delegate = self
       // var arr:NSArray = self.navigationController!.viewControllers
        tableVw.dataSource = self
        let nib = UINib(nibName: "chatRoomMsgOther", bundle: nil)
        tableVw.registerNib(nib, forCellReuseIdentifier: "cell")
        tableVw.rowHeight = UITableViewAutomaticDimension
        tableVw.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI))
        tableVw.estimatedRowHeight = 60.0
        self.tabBarController?.tabBar.hidden = true
        sendChatBtn.layer.cornerRadius = 5.0
        inputContainer.layer.cornerRadius = 4.0
        let tapDismissKeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapDismissKeyboard)
        appDelegate.chatRoom = self
        DBChats.updateGroupNotifCount(idGroup, count: -1)
        let rightBtn : UIBarButtonItem = UIBarButtonItem(title: "Details", style: UIBarButtonItemStyle.Plain, target: self, action: "DetailsAction")
        rightBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = rightBtn
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Arial", size: 18)!], forState: UIControlState.Normal)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self);
    }
    func keyboardWillShow(sender: NSNotification){
        let frame = (sender.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.marginVal = frame.height
        resizeWithKeyboard()
    }
    func keyboardWillHide(sender: NSNotification){
        //let frame = (sender.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.marginVal = 0
        resizeWithKeyboard()
    }
    func DetailsAction(){
        performSegueWithIdentifier("showchatGrouDetailsVC", sender: self)
    }
    override func viewWillAppear(animated: Bool){
        self.title = self.nameGroup
        soapObject = GetCommuniqueBoxNotes()
        serviceResponse(soapObject)
            soapObject.pageTarget = self
            soapObject.groupID = self.idGroup
            let params:[String] = [publicSuidSession, Utility.intToSuid(idGroup)]
            soapObject.request(params)
            let serviceConnector:WSSoapConnector = WSSoapConnector()
            serviceConnector.callAPI(soapObject)
    }
    override func viewWillDisappear(animated: Bool) {
        appDelegate.chatRoom = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func sendChatBtnAction(sender: AnyObject){
        if chatInputField.text != ""
        {
            let params:[String] = [publicSuidSession, Utility.intToSuid(idGroup), chatInputField.text!]
            soapObjSendMsg = SendNoteToCommuniqueBox()
            var chatMsg:structChatMsg = structChatMsg()
            chatMsg.text = chatInputField.text!
            chatMsg.type = Const.OWN_MSG
            chatMsg.user.name = userName
            chatMsg.user.id = Utility.suidToInt(publicSuidUser)
            chatMsg.groupID = idGroup
            soapObjSendMsg.strctMsg = chatMsg
            soapObjSendMsg.request(params)
            let connector:WSSoapConnector = WSSoapConnector()
            connector.callAPI(soapObjSendMsg)
            addMessageInRoom(chatMsg)
            chatInputField.text = ""
        }
        
        //resizeWithKeyboard(true)
    }
    func resizeWithKeyboard(){
        UIView.animateWithDuration(0.35, animations: {()-> Void in
            self.bottomMargin.constant = self.marginVal
            self.view.layoutIfNeeded()
        })
    }
    func addMessageInRoom(chatMsg:structChatMsg){
        if chatMsg.groupID == idGroup
        {
            if !self.checkForDuplicateMsg(chatMsg.id) {
//                self.chatList.append(chatMsg)
                self.chatList.insert(chatMsg, atIndex: 0)
                self.tableVw.reloadData()
                scrollToBottom(true)
            }
        }
        else
        {
            unreadChatMsg = [1:2]
            //var test:Int = unreadChatMsg[1]!
        }
    }
    func scrollToBottom(animated:Bool){
//        if self.chatList.count > 0
//        {
//            self.tableVw.scrollToRowAtIndexPath(NSIndexPath(forRow: self.chatList.count - 1, inSection: 0), atScrollPosition: .Bottom, animated: false)
//            
//            let delay = 0.1 * Double(NSEC_PER_SEC)
//            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
//            
//            dispatch_after(time, dispatch_get_main_queue(),
//                {
//                    self.tableVw.scrollToRowAtIndexPath(NSIndexPath(forRow: self.chatList.count - 1, inSection: 0), atScrollPosition: .Bottom, animated: false)
//            })
//        }
    }
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.chatList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let row:Int = indexPath.row as Int
        let cell:chatRoomMsg = tableView.dequeueReusableCellWithIdentifier("cell") as! chatRoomMsg
        cell.transform =  CGAffineTransformMakeRotation(CGFloat(M_PI))
        cell.backgroundColor = UIColor.clearColor()
        cell.createMsg(self.chatList[row])
        return cell
    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
//        tableView.deselectRowAtIndexPath(indexPath, animated: true)
//    }
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool{
        return true
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        if segue.identifier == "showchatGrouDetailsVC"
        {
            if let destinationVC = segue.destinationViewController as? chatGroupDetailsVC
            {
                destinationVC.groupID = self.idGroup
            }
        }
    }
    func serviceResponse(target: _IFaceAPI){
        //let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        
        self.chatList = DBChats.getMessages(self.idGroup)
        self.tableVw.reloadData()
        self.scrollToBottom(false)
    }
    func checkForDuplicateMsg(id:Int)->Bool{
        var returnVal:Bool = false
        for var i:Int = chatList.count-1; i>=0; i-- {
            if id == chatList[i].id {
                returnVal = false
                break
            }
        }
        return returnVal
    }
}
