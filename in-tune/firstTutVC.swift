
import UIKit

class firstTutVC: UIViewController, UIPageViewControllerDataSource
{
    var pageViewController: UIPageViewController!
    var pageImages: NSArray!
    var flagForSelectingPageImages: Int! //which pageImages to show: Tut1 = 0 & Tut2 = 1 
    
    @IBOutlet var doneBtn: UIButton!
    @IBAction func exitTut(sender: AnyObject)
    {
       self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        if flagForSelectingPageImages == nil
        {
            pageImages = NSArray(objects: "intro1", "intro2", "intro3")
        }
        else
        {
            pageImages = NSArray(objects: "intune-tut2-scr1", "intune-tut2-scr2","intune-tut2-scr3")
        }
        pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("firstPVC") as! UIPageViewController
        self.pageViewController.dataSource = self
        let startVC = self.viewControllerAtIndex(0) as firstTutContentVC
        let viewControllers = NSArray(object: startVC)
        self.pageViewController.setViewControllers((viewControllers as! [UIViewController]), direction: .Forward, animated: true, completion: nil)
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        doneBtn.layer.cornerRadius = 3
        doneBtn.layer.borderWidth = 1
        doneBtn.layer.borderColor = UIColor(netHex: 0xffffff).CGColor
        doneBtn.hidden = true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func viewControllerAtIndex(Index:Int) -> firstTutContentVC
    {
        let vc: firstTutContentVC = self.storyboard?.instantiateViewControllerWithIdentifier("firstTutContentVC") as! firstTutContentVC
        vc.imageFile = self.pageImages[Index] as! String
        vc.pageIndex = Index
        return vc
    }
    
    //MARK:- Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let vc = viewController as! firstTutContentVC
        
        var index = vc.pageIndex as Int
        
        if (index == 0 || index == NSNotFound)
        {
            return nil
        }
        
        index--
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        let vc = viewController as! firstTutContentVC
        var index = vc.pageIndex as Int
        
        if (index == NSNotFound)
        {
            return nil
        }
        index++
        if index == 3
        {
            doneBtn.hidden = false
            self.view.bringSubviewToFront(doneBtn)
        }
        if (index == self.pageImages.count)
        {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return self.pageImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
}
