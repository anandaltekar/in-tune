
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

var userName:                   String!
var fbAccessToken:              String!
var fbID:                       String!
var fbEmail:                    String!
var fbFirstName:                String!
var fbLastName:                 String!
var fbLink:                     String!
var fbGender:                   String!
var fbPhotos:                   NSDictionary!
var fbPhotos2:                  NSDictionary!
var fbProfilePictureURL:        String!
var publicSuidSession:          String = ""
var publicSuidUser:             String = ""
var publicUserType:             Int = Const.GUEST_USER
var selectedConcert:            String = ""
var fbpp:                       NSDictionary!
var fbpp2:                      NSDictionary!
var fbLocation:                       NSDictionary!
var unreadChatMsg =             [1:2]
var flagForMatchToShowTabIndex: Int = 0
var flagToCheckFirstUse:        Int = 1
var publicChatUpdates:          Bool = false
var navigateTo:                 Int = 0

class ViewController: UIViewController, FBSDKLoginButtonDelegate, _IFaceCallTarget
{
    @IBOutlet var hostLoginBtn: UIButton!
    var soapObject: Registration!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var loginButton = FBSDKLoginButton()
    //var navHeight:CGFloat!
    //var statusHeight:CGFloat!
    
    @IBAction func hostSelect(sender: UIButton)
    {
        if publicSuidSession == ""
        {
            performSegueWithIdentifier("showHostEntryCodeVC", sender: self)
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        //navHeight = self.navigationController?.navigationBar.frame.height
        //statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.userDefaults.setValue(flagToCheckFirstUse, forKey: "firstUse")
        loginButton.readPermissions = ["public_profile", "email"]
        loginButton.delegate = self
        self.view.addSubview(loginButton)
        
        if Reachability.isConnectedToNetwork() == true
        {
            print("connection = ", Reachability.isConnectedToNetwork().boolValue)
            
            if (userDefaults.valueForKey("suidSession") as? String != nil)
            {
                publicSuidSession = userDefaults.valueForKey("suidSession") as! String
                publicSuidUser = userDefaults.valueForKey("suidUser") as! String
                if userDefaults.objectForKey("suidConcert") != nil
                {
                    selectedConcert = userDefaults.valueForKey("suidConcert") as! String
                }
                if userDefaults.objectForKey("userType") != nil{
                    let strType:String = userDefaults.valueForKey("userType") as! String
                    publicUserType = Int(strType)!
                }
                else {
                    publicUserType = Const.HOST_USER
                }
                
                if userDefaults.objectForKey("fbDetails") != nil {
                    let fbDetailsStr:String = userDefaults.valueForKey("fbDetails") as! String
                    var fbDetailsArr:[String] = decodeFBDetails(fbDetailsStr)
                    
                    fbID = fbDetailsArr[0]
                    userName = fbDetailsArr[1]
                    fbAccessToken = fbDetailsArr[2]
                    fbID = fbDetailsArr[3]
                    fbEmail = fbDetailsArr[4]
                    fbFirstName = fbDetailsArr[5]
                    fbLastName = fbDetailsArr[6]
                    fbLink = fbDetailsArr[7]
                    fbProfilePictureURL = fbDetailsArr[8]
                }
            }
            else
            {
                if (FBSDKAccessToken.currentAccessToken() == nil)
                {
                    print("Not logged in..")
                }
                else
                {
                    self.loginButton = FBSDKLoginButton()
                    print("Logged in")
                    fbAccessToken = FBSDKAccessToken.currentAccessToken().tokenString
                    reqFBDdetails()
                }
            }
        }
        else
        {
            print("needs internet connection")
            //display alert with retry option
            let alertController = UIAlertController(title: "Oops!", message: "connection is not active, try again later", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Damnit!", style: .Default, handler: {Void in alertController.dismissViewControllerAnimated(false, completion: nil)}
                ))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        if userDefaults.valueForKey("startTimeUse") != nil
        {
            SRTimer.startSongRequestTimer()
        }
        self.checkDBTables()
        hostLoginBtn.layer.cornerRadius = 5
        hostLoginBtn.layer.borderWidth = 2
        hostLoginBtn.layer.borderColor = UIColor(netHex: 0xe01e36).CGColor
        print(publicSuidSession)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        if publicSuidSession != "" && publicUserType == Const.GUEST_USER
        {
            self.performSegueWithIdentifier("loginSuccess", sender: nil)
        }
        else if publicSuidSession != "" && publicUserType == Const.HOST_USER
        {
            performSegueWithIdentifier("showHostEntryCodeVC", sender: self)
        }
        loginButton.frame = CGRectMake(hostLoginBtn.frame.origin.x-25, hostLoginBtn.frame.origin.y - hostLoginBtn.frame.height-28, hostLoginBtn.frame.width+50, hostLoginBtn.frame.height+5)
        loginButton.hidden = false
        self.view.bringSubviewToFront(loginButton)
    }
    
    func reqFBDdetails()
    {
        let reqFBDetails = FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"id, first_name, last_name, email, gender, link, picture.type(large)"], tokenString: fbAccessToken, version: nil, HTTPMethod: "GET")
        
        reqFBDetails.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil)
            {
                fbID             = result.valueForKey("id") as? String
                fbEmail          = result.valueForKey("email") as? String
                fbFirstName      = result.valueForKey("first_name") as? String
                fbLastName       = result.valueForKey("last_name") as? String
                //fbFirstName      = "Derreck"
                //fbLastName       = "Williams"
                fbLink           = result.valueForKey("link") as? String
                //fbLocation       = result.valueForKey("location") as! NSDictionary
                fbGender         = result.valueForKey("gender") as? String
                fbpp             = result.valueForKey("picture") as! NSDictionary
                fbpp2            = fbpp.valueForKey("data") as! NSDictionary
                fbProfilePictureURL = "https://graph.facebook.com/\(fbID)/picture"
//                fbPhotos         = result.valueForKey("photos") as! NSDictionary
                
//                println("\(fbID), \(fbEmail), \(fbFirstName), \(fbLastName), \(fbPhotos). \(fbLink), \(fbProfilePictureURL)")
                print("\(fbLink)")
                userName = fbFirstName + " " + fbLastName

                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let params: [String] = [fbID, fbProfilePictureURL, fbLink, fbEmail, userName, "1", appDelegate.currentDeviceToken, "ios", "b892b818-6228-4088-ab9d-3b1737a2bce8"]
                self.soapObject = Registration()
                self.soapObject.request(params)
                self.soapObject.pageTarget = self
                let connector: WSSoapConnector = WSSoapConnector()
                connector.callAPI(self.soapObject)
            }
            else
            {
                print("error \(error)", terminator: "")
            }
        })
        //else if internet connection fails, show alert asking to retry
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        if error == nil
        {
            if result.token != nil {
                print("Login Successful")
                fbAccessToken = result.token.tokenString
                reqFBDdetails()
            }
        }
        else if result.isCancelled
        {
            //handle cancellations, dispolay alert
        }
        else
        {
            print(error.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
    {
        print("User logged out")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if soapObject.respCode == 0
        {
            let fbDetails:String = encodeFBDetails([fbID, userName, fbAccessToken, fbID, fbEmail, fbFirstName, fbLastName, fbLink, fbProfilePictureURL])
            publicSuidSession = self.soapObject.suidSession
            publicSuidUser = self.soapObject.suidUser
            userDefaults.setValue(publicSuidSession, forKey: "suidSession")
            userDefaults.setValue(publicSuidUser, forKey: "suidUser")
            userDefaults.setValue(String(soapObject.userType), forKey: "userType")
            userDefaults.setValue(fbDetails, forKey: "fbDetails")
            if self.soapObject.userType == 0
            {
                if userDefaults.valueForKey("firstUse") as! Int == 0
                {
                    self.performSegueWithIdentifier("loginSuccess", sender: nil)
                }
                else
                {
                    flagToCheckFirstUse = 0
                    userDefaults.setValue(flagToCheckFirstUse, forKey: "firstUse")
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("firstTutVC") as! firstTutVC
                    self.presentViewController(vc, animated: false, completion: nil)
                }
            }
            else
            {
                self.performSegueWithIdentifier("showHostEntryCodeVC", sender: self)
            }
        }
    }
    
    func encodeFBDetails(detailsArr:[String])->String
    {
        return detailsArr.joinWithSeparator(Const.STR_JOINR)
    }
    
    func decodeFBDetails(Str:String)->[String]
    {
        return Str.componentsSeparatedByString(Const.STR_JOINR)
    }
    
    func checkDBTables()
    {
        let (resultSet, _) = SD.existingTables();
        let tableCount:Int  = resultSet.count
        if tableCount==0
        {
            DBChats.createChatTables()
        }
    }
}