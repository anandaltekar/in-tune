//
//  hostSLVC.swift
//  in-tune
//
//  Created by Anand Altekar on 01/09/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostSLVC: UIViewController, UITableViewDataSource, UITableViewDelegate,  _IFaceCallTarget
{
    @IBOutlet weak var tableView: UITableView!
    
    var navHeight           : CGFloat!
    var statusHeight        : CGFloat!
    var soapObject          : GetConcertDetails!
    var soapObjectQ         : PlayNext!
    var soapObjectKill      : KillSongOrPlaylist!
    var indexOfCell         : NSIndexPath!          //indexpath for selectd song
    var currentQ            : Int = -1              //To assign and clear checkmarks
    var refreshControl      : UIRefreshControl!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        navHeight = self.navigationController?.navigationBar.frame.height
        statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        callToGetConcertDetails()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let backBtn : UIBarButtonItem = UIBarButtonItem(title: "Log Out", style: UIBarButtonItemStyle.Plain, target: self, action: "hostLogOut")
        backBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = backBtn
        
        let groupsBtn : UIBarButtonItem = UIBarButtonItem(title: "Groups", style: UIBarButtonItemStyle.Plain, target: self, action: "groupsBtnAction")
        groupsBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = groupsBtn
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Reloading...")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refresh()
    {
        callToGetConcertDetails()
    }
    
    func hostLogOut()
    {
        Utility.resetPublicVars()
//        self.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
    
    func groupsBtnAction()
    {
        performSegueWithIdentifier("showHostGroupsVC", sender: self)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool)
    {
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.soapObject.ConcertDetails.SongList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("hostSongListItem", forIndexPath: indexPath) as! hostSongListCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        if self.soapObject.ConcertDetails.SongList[indexPath.row].nKilledStatus == Const.SONG_KILLED
        {
//            cell.backgroundColor = UIColor.lightGrayColor()
//            cell.accessoryType = .None
            cell.setCellType(2)
        }
        else if indexPath.row == currentQ
        {
//            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
//            cell.backgroundColor = UIColor.clearColor()
            cell.setCellType(1)
        }
        else
        {
            cell.setCellType(0)
        }
        cell.loadData(self.soapObject.ConcertDetails.SongList[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return false
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        //
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
    {
        let queueAction = UITableViewRowAction(style: .Normal, title: "Queue",
            handler: { (action: UITableViewRowAction, indexPath: NSIndexPath) in
                print("Q pressed")
                self.indexOfCell = indexPath
                self.confirmQueue()
            }
        );
        queueAction.backgroundColor = UIColor(netHex: 0x71985e)
        
        let killAction = UITableViewRowAction(style: .Normal, title: " Kill ",
            handler: { (action: UITableViewRowAction, indexPath: NSIndexPath) in
                print("kill pressed")
                self.indexOfCell = indexPath
                self.confirmKill()
            }
        );
        killAction.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.40)
        return [killAction, queueAction]
    }
    
    func confirmQueue()
    {
        let alert = UIAlertController(title: "Play next", message: "do you really want to notify all users that this song will play next?", preferredStyle: .ActionSheet)
        
        let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
            self.callToPlayNext()
            self.tableView.editing = false
        })
        
        let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil )
        alert.addAction(confirmAction)
        alert.addAction(CancelAction)
        
        self.presentViewController(alert, animated: true, completion:nil)
    }
    
    func confirmKill()
    {
        let alert = UIAlertController(title: "Kill Song", message: "do you really want to make this song unavailable henceforth?", preferredStyle: .ActionSheet)
        
        let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
            self.callToKillSongOrPlaylist()
            self.tableView.editing = false
        })
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(CancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func callToGetConcertDetails()
    {
        soapObject = GetConcertDetails()
        soapObject.request([publicSuidSession, selectedConcert])
        soapObject.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(soapObject)
    }
    
    func callToPlayNext()
    {
        self.soapObjectQ = PlayNext()
        let songID = Utility.intToSuid(self.soapObject.ConcertDetails.SongList[self.indexOfCell.row].id)
        self.soapObjectQ.request([publicSuidSession, songID])
        //        self.soapObjectQ.request([publicSuidSession, selectedConcert])
        self.soapObjectQ.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(self.soapObjectQ)
    }
    
    func callToKillSongOrPlaylist()
    {
        let songOrPlaylistFlag:String = "1" //1 for a song, 0 for a playlist
        let songID = Utility.intToSuid(self.soapObject.ConcertDetails.SongList[self.indexOfCell.row].id)
        self.soapObjectKill = KillSongOrPlaylist()
        self.soapObjectKill.request([publicSuidSession, songID, songOrPlaylistFlag])
        self.soapObjectKill.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(self.soapObjectKill)
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if target.methodName == "GetConcertDetails"
        {
            if soapObject.respCode == 0
            {
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
            else
            {
                //display error alert
            }
        }
        else if target.methodName == "PlayNext"
        {
            if soapObjectQ.respCode == 0
            {
                //clear all checkmarks
                if currentQ != -1
                {
                    let qCell = NSIndexPath(forRow: currentQ, inSection: 0)
                    let clearCell = self.tableView.cellForRowAtIndexPath(qCell) as! hostSongListCell
                    clearCell.setCellType(0)
                }
                currentQ = self.indexOfCell.row
                let cell = self.tableView.cellForRowAtIndexPath(self.indexOfCell) as! hostSongListCell
                cell.setCellType(1)
                //cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                //set custom accessory to show next playing song
            }
            else
            {
                //display alert
            }
        }
        else if target.methodName == "KillSongOrPlaylist"
        {
            if soapObjectKill.respCode == 0
            {
                let cell = self.tableView.cellForRowAtIndexPath(self.indexOfCell) as! hostSongListCell
                self.soapObject.ConcertDetails.SongList[self.indexOfCell.row].nKilledStatus = Const.SONG_KILLED
                cell.setCellType(2)
                //cell?.backgroundColor = UIColor.grayColor()
                //set custom background colour here for killed song
            }
            else
            {
                //display alert
            }
            
        }
    }

}
