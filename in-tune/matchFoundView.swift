
import Foundation

class matchFoundView : UIView, _IFaceCallTarget {
    
    //    @IBOutlet var userImage: UIImageView!
    //    @IBOutlet var bottomBtnContainer: UIView!
    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var buttonContainer: UIView!
    @IBOutlet var userName: UILabel!
    var parentView:FDViewController!
    var view: UIView!
    var strGroup:structChatMsgGroup!
    var message:String = "You are intuned for song "
    var userNameTxt:String = ""
    var notificationID:Int = 0
    var requestSent:Int = 0
    var soapObject:SetAcceptRejectMatchesFound!
    
    @IBOutlet var matchLabel: UILabel!
    //var matchNotif:structNotification!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xsetup()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xsetup()
    }
    
    func xsetup()
    {
        let nibName:String = "matchFoundView"
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: nibName, bundle: bundle)
        self.view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.view.frame = CGRectMake(0, 0, 0, 0)
        self.view.layer.cornerRadius = 8.0
        self.view.backgroundColor = UIColor(netHex: 0xffffff)
        self.clipsToBounds = true
        self.addSubview(view)
        userImage.alpha = 0
        //self.userName.layer.cornerRadius = 4
        buttonContainer.alpha = 0
        buttonContainer.layer.cornerRadius = 7
        buttonContainer.layer.borderWidth = 1
        buttonContainer.layer.borderColor = UIColor(netHex: 0xa56704).CGColor
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor(netHex: 0xa56704).CGColor
    }
    
    @IBAction func AcceptBtnAction(sender: UIButton) {
        if requestSent == 0
        {
            callToSetAcceptRejectMatchesFound(Const.REQ_ACCEPT)
            flagForMatchToShowTabIndex = 1
            requestSent = 1
        }
    }
    
    @IBAction func ignoreBtnAction(sender: UIButton) {
        if requestSent == 0 {
            callToSetAcceptRejectMatchesFound(Const.REQ_REJECT)
            requestSent = 1
        }
    }
    
    func callToSetAcceptRejectMatchesFound(nFlagSet:String)
    {
        DBChats.updateNotificationState(self.notificationID, state:Const.READ)
        UIView.animateWithDuration(0.2, animations: {
            self.alpha = 0
            self.parentView.changePositions(self.frame)
            }, completion: { finished in
                self.removeFromSuperview()
        })
        
        let params:[String] = [publicSuidSession, Utility.intToSuid(strGroup.id), nFlagSet]
        soapObject = SetAcceptRejectMatchesFound()
        soapObject.pageTarget = self
        soapObject.selectedVal = nFlagSet
        soapObject.request(params)
        let soapConnector:WSSoapConnector = WSSoapConnector()
        soapConnector.callAPI(soapObject)
    }
    
    func showContent()
    {
        UIView.animateWithDuration(0.2, animations:
            {
            self.userImage.alpha = 1
            self.buttonContainer.alpha = 1
            Utility.downloadFBPP(self.strGroup.userList[0].fbProfUrl, imgView: self.userImage, isLargePic: true)
            self.matchLabel.text = self.message
            self.userName.text = self.userNameTxt
            //self.userName.text = self.
        })
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        requestSent = 0
        if soapObject.respCode == 0
        {
            self.parentView.mainTabBar.addBadge()
            //change notification state - unread to read
            DBChats.updateNotificationState(self.notificationID, state:Const.READ)
            
            // Changing user status - Unactive to Active - who have already accepted request
            let respReceivedGroup:structChatMsgGroup = soapObject.chatGroup
            if soapObject.selectedVal == Const.REQ_ACCEPT
            {
                if respReceivedGroup.userList.count==0 {
                    //you are first acceptor
                    strGroup.status = Const.INACTIVE
                    DBChats.addGroups([strGroup])
                }
                else {
                    //you are not first acceptor
                    strGroup.badgeCount = 1
                    strGroup.status = Const.ACTIVE
                    strGroup.userList = Utility.mergeUsers(strGroup.userList, newUsers: respReceivedGroup.userList)
                    DBChats.addGroups([strGroup], count:1)
                }
            }
        }
        else {
            //change notification state - unread to read
           // DBChats.updateNotificationState(self.notificationID, state:Const.UNREAD)
        }
    }
}
