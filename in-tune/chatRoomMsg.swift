
import UIKit

class chatRoomMsg: UITableViewCell
{
    @IBOutlet var otherName: UILabel!
    @IBOutlet var otherText: UITextView!
    @IBOutlet var OthUsrContnr:UIView!
    @IBOutlet var ownName: UILabel!
    @IBOutlet var ownText: UITextView!
    @IBOutlet var ownContnr:UIView!
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var otherBgImg: UIImageView!
    @IBOutlet var hostChatMsg: UIView!
    @IBOutlet var hostChatLbl: UILabel!
    @IBOutlet var hostChatText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hostChatMsg.layer.cornerRadius = 4.0
        ownContnr.hidden = true
        OthUsrContnr.hidden = true
        hostChatMsg.hidden = true
        var bgImg = UIImage(named: "own_chat_bg.png")
        bgImg = bgImg?.resizableImageWithCapInsets(UIEdgeInsetsMake(10, 10, 20, 20))
        bgImage.image = bgImg
       
        var otherBgImgs = UIImage(named: "other_chat_bg.png")
        otherBgImgs = otherBgImgs?.resizableImageWithCapInsets(UIEdgeInsetsMake(10, 10, 20, 20))
        otherBgImg.image = otherBgImgs
        
        otherText.contentInset = UIEdgeInsetsMake(-4,-4,0,0);
        ownText.contentInset = UIEdgeInsetsMake(-4,-4,0,0);
        
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func createMsg(data:structChatMsg)
    {
        if data.type == Const.OWN_MSG
        {
            ownContnr.hidden = false
            OthUsrContnr.hidden = true
            hostChatMsg.hidden = true
            ownText.text = data.text
            ownName.text = data.user.name
            otherText.text = ""
            hostChatText.text = ""
        }
        else if data.type == Const.OTHER_MSG
        {
            ownContnr.hidden = true
            OthUsrContnr.hidden = false
            hostChatMsg.hidden = true
            otherText.text = data.text
            otherName.text = data.user.name
            ownText.text = ""
            hostChatText.text = ""
        }
        else {
            ownContnr.hidden = true
            OthUsrContnr.hidden = true
            hostChatMsg.hidden = false
            hostChatText.text = data.text
            hostChatLbl.text = data.user.name
            ownText.text = ""
            otherText.text = ""
        }
    }

}
