
import UIKit

class matchVC: UIViewController, _IFaceCallTarget
{
    var soapObject:SetAcceptRejectMatchesFound!
    var matchUserImgWidth:Int = 55
    var matchUserImgMargin:Int = 8
    var structGroup:structChatMsgGroup!
    var notificationID:Int = 0
    var requestSent:Int = 0
    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var matchUserImgContainer: UIView!
    @IBOutlet var bottomBtnContainer: UIView!
    @IBOutlet var songTitle: UILabel!
    
    @IBAction func acceptMatch(sender: AnyObject)
    {
        if requestSent == 0
        {
            callToSetAcceptRejectMatchesFound(Const.REQ_ACCEPT)
            flagForMatchToShowTabIndex = 1
            requestSent = 1
        }
    }
    @IBAction func ignoreMatch(sender: AnyObject)
    {
        if requestSent == 0 {
            callToSetAcceptRejectMatchesFound(Const.REQ_REJECT)
            requestSent = 1
        }
    }
    override func viewDidLoad()
    {
        requestSent = 0
        super.viewDidLoad()
        Utility.pageGredient(self.view)
    }
    override func viewWillAppear(animated: Bool)
    {
        bottomBtnContainer.layer.cornerRadius = 7
        bottomBtnContainer.layer.borderWidth = 2
        bottomBtnContainer.layer.borderColor = UIColor(netHex: 0xffffff).CGColor
        userImage.layer.cornerRadius = userImage.bounds.size.height/2
        Utility.downloadFBPP(fbID, imgView: userImage, isLargePic: true)
        loadPageData()
    }
    
    func loadPageData(){
        let imgCount:Int = structGroup.userList.count
        let containerWidth:CGFloat = matchUserImgContainer.layer.frame.width
        let imgsSpace:Int = imgCount * (matchUserImgWidth+matchUserImgMargin)-matchUserImgMargin
        let leftSpacing:CGFloat = (containerWidth-CGFloat(imgsSpace))/2
        songTitle.text = "For Song - \(structGroup.name.uppercaseString)"
        for view in self.matchUserImgContainer.subviews {
            view.removeFromSuperview()
        }
        
        for var i:Int=0; i<imgCount; i++ {
            let imgVw:UIImageView = UIImageView()
            imgVw.frame = CGRect(x: (i*(matchUserImgWidth+matchUserImgMargin)+Int(leftSpacing)), y: 0, width: matchUserImgWidth, height: matchUserImgWidth)
            imgVw.layer.cornerRadius = imgVw.bounds.size.height/2
            imgVw.clipsToBounds = true
            matchUserImgContainer.addSubview(imgVw)
            Utility.downloadFBPP(structGroup.userList[i].fbProfUrl, imgView: imgVw, isLargePic: false)
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    func callToSetAcceptRejectMatchesFound(nFlagSet:String)
    {
        let params:[String] = [publicSuidSession, Utility.intToSuid(structGroup.id), nFlagSet]
        soapObject = SetAcceptRejectMatchesFound()
        soapObject.pageTarget = self
        soapObject.selectedVal = nFlagSet
        soapObject.request(params)
        let soapConnector:WSSoapConnector = WSSoapConnector()
        soapConnector.callAPI(soapObject)
    }
    func serviceResponse(target: _IFaceAPI)
    {
        if soapObject.respCode == 0
        {
            //change notification state - unread to read
            DBChats.updateNotificationState(self.notificationID, state:Const.READ)
            
            // Changing user status - Unactive to Active - who have already accepted request
            let respReceivedGroup:structChatMsgGroup = soapObject.chatGroup
            if soapObject.selectedVal == Const.REQ_ACCEPT
            {
                if respReceivedGroup.userList.count==0 {
                    //you are first acceptor
                    structGroup.status = Const.INACTIVE
                    DBChats.addGroups([structGroup])
                }
                else {
                    //you are not first acceptor
                    structGroup.badgeCount = 1
                    structGroup.userList = Utility.mergeUsers(structGroup.userList, newUsers: respReceivedGroup.userList)
                    DBChats.addGroups([structGroup], count:1)
                }
            }
            
            //checking if any pending notification of kind MATCH-FOUND
            var notifList:[structNotification] = DBChats.getUnreadNotifs(Const.NOTIF_MATCH_FOUND)
            if notifList.count > 0 {
                // adding new notification
                requestSent = 0
                self.notificationID = notifList[0].id
                self.structGroup = Utility.notificationToGroup(notifList[0])
                self.loadPageData()
            }
            else {
                self.dismissViewControllerAnimated(false, completion: nil)
            }
        }
        else
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
}
