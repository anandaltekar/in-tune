//
//  hostEntryCodeVC.swift
//  in-tune
//
//  Created by Anand Altekar on 17/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostEntryCodeVC: UIViewController, _IFaceCallTarget
{
    var hostSoapObject:VerifyHostUser!
    
    @IBOutlet weak var hostCode: UITextField!
    @IBOutlet var hostNameContainer: UIView!
    @IBOutlet var entryCodeContainer: UIView!
    @IBOutlet var inputContainer: UIView!
    @IBOutlet var verifyBtn: UIButton!
    
    @IBAction func hostCodeEntered(sender: UIButton)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let params:[String] = [appDelegate.currentDeviceToken, "ios", hostCode.text!]
        hostSoapObject = VerifyHostUser()
        hostSoapObject.pageTarget = self
        hostSoapObject.request(params)
        let soapConnector:WSSoapConnector = WSSoapConnector()
        soapConnector.callAPI(hostSoapObject)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        if publicSuidSession != "" && publicUserType == Const.HOST_USER
        {
            performSegueWithIdentifier("showHSLVC", sender: self)
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let backBtn : UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "backBtnAction")
        backBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Arial", size: 18)!], forState: UIControlState.Normal)
        self.navigationItem.leftBarButtonItem = backBtn
        Utility.pageGredient(self.view)
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        
        hostNameContainer.layer.cornerRadius = 6.0
        entryCodeContainer.layer.cornerRadius = 6.0
        inputContainer.layer.cornerRadius = 3.0
        verifyBtn.layer.cornerRadius = 3.0
        verifyBtn.layer.borderWidth = 1
        verifyBtn.layer.borderColor = UIColor.whiteColor().CGColor
        //Dismiss Keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func backBtnAction()
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if hostSoapObject.respCode == 0
        {
            performSegueWithIdentifier("showHSLVC", sender: self)
        }
        else
        {
            let alert = UIAlertController(title: "Oops!", message: "You probabely entered a wrong code, please enter again", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
}