//
//  hostSongListCell.swift
//  in-tune
//
//  Created by Anand Altekar on 19/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostSongListCell: UITableViewCell
//class hostSongListCell: SBGestureTableViewCell
{
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var numberOfRequests: UILabel!
    @IBOutlet weak var numberOfMatches: UILabel!
    @IBOutlet var cellHighlighter: UIView!
    
    func loadData(data:structSongDetails)
    {
        songName.text = data.sSongsName
        //songName.textColor = UIColor.blackColor()
        artistName.text = data.sSongArtist
        //artistName.textColor = UIColor.lightGrayColor()
        numberOfRequests.text = String(data.nTotalReqFound)
        numberOfMatches.text = String(data.nMatchesFound)
    }
    
    func setCellType(type:Int){
        if type == 1 {
            self.contentView.sendSubviewToBack(cellHighlighter)
            cellHighlighter.backgroundColor = UIColor(netHex: 0x71985e)
        }
        else if type == 2 {
            self.contentView.bringSubviewToFront(cellHighlighter)
            cellHighlighter.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.40)
        }
        else {
            self.contentView.sendSubviewToBack(cellHighlighter)
            cellHighlighter.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.15)
        }
    }
}
