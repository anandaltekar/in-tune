//
//  VerifyHostUser.swift
//  in-tune
//
//  Created by Anand Altekar on 20/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import Foundation

class VerifyHostUser: NSObject, NSXMLParserDelegate, _IFaceAPI
{
    var methodName: String = "VerifyHostUser"
    var pageTarget: _IFaceCallTarget?
    var soapEnv: String = ""
    var respCode: Int = 10
    
    func request(params:[String])
    {
        self.soapEnv = "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><VerifyHostUser xmlns='http://tempuri.org/'><sDeviceOrGCMCode>\(params[0])</sDeviceOrGCMCode><sDeviceOS>\(params[1])</sDeviceOS><sVerifyCode>\(params[2])</sVerifyCode></VerifyHostUser></soap:Body></soap:Envelope>"
    }
    
    func response(response responseData: NSMutableData)
    {
        do {
            let xmlDoc = try AEXMLDocument(xmlData: responseData)
            respCode=Int(xmlDoc.root["soap:Body"]["\(methodName)Response"]["\(methodName)Result"]["_jResponseCode"].value!)!
            let dictData:AEXMLElement = xmlDoc.root["soap:Body"]["\(methodName)Response"]["\(methodName)Result"]["_objResponse"]

            publicSuidSession = dictData["_suidSession"].value!
            publicSuidUser = dictData["_suidUser"].value!
            selectedConcert = dictData["_suidConcert"].value!
            publicUserType = Const.HOST_USER
            //set const.usertype = host
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setValue(publicSuidSession, forKey: "suidSession")
            userDefaults.setValue(publicSuidUser, forKey: "suidUser")
            userDefaults.setValue(selectedConcert, forKey: "suidConcert")
            
//            <_isHostUser>int</_isHostUser>

            pageTarget?.serviceResponse(self)
        } catch _ {
        }
    }
    
}
