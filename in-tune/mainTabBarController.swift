//
//  mainTabBarController.swift
//  in-tune
//
//  Created by Anand Altekar on 30/07/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class mainTabBarController: UITabBarController
{
    var soapObject: LogOut!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if flagForMatchToShowTabIndex == 0
        {
            self.selectedIndex = 1
        }
        else
        {
            self.selectedIndex = 1
            flagForMatchToShowTabIndex = 0
        }
        
        let rightBtn = UIButton()
        rightBtn.setImage(UIImage(named: "setting.png"), forState: UIControlState.Normal)
        rightBtn.targetForAction("SettingAction", withSender: self)
        rightBtn .addTarget(self, action: "SettingAction", forControlEvents: UIControlEvents.TouchUpInside)
        rightBtn.frame = CGRectMake(0, 0, 25, 25)
        rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let barButton = UIBarButtonItem(customView: rightBtn)
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        let logo = UIImage(named: "navBarLogo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = imageView
        
        let tabBar = self.tabBar
        let feedBackIcon = UIImage(named: "feedback.png")
        let musicIcon = UIImage(named: "music.png")
        let chatIcon = UIImage(named: "chat.png")
        
        (tabBar.items![0] ).imageInsets = UIEdgeInsets(top:7, left:0, bottom:-7, right:0)
        (tabBar.items![1] ).imageInsets = UIEdgeInsets(top:5, left:0, bottom:-5, right:0)
        (tabBar.items![2] ).imageInsets = UIEdgeInsets(top:6, left:0, bottom:-6, right:0)
        
        (tabBar.items![0] ).image = feedBackIcon?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (tabBar.items![1] ).image = musicIcon?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        (tabBar.items![2] ).image = chatIcon?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(UIColor(netHex: 0x000000).colorWithAlphaComponent(0.25), size: tabBarItemSize).resizableImageWithCapInsets(UIEdgeInsetsZero)
        appDelegate.mainTabBar = self
    }
    
    override func viewWillAppear(animated: Bool)
    {
        appDelegate.mainTabBar = self
        addBadge()
    }
    func SettingAction()
    {
        self.performSegueWithIdentifier("settingPage", sender: self)
    }
    
    func addBadge()
    {
        print("$$@@##@@$$-mainTabBar-addBadge")
        let groupCount:[[Int:Int]] = DBChats.getUnreadGroups()
        let tabItem:UITabBarItem = self.tabBar.items![2] 
        if groupCount.count > 0 {
            tabItem.badgeValue = "\(groupCount.count)"
        }
        else {
            tabItem.badgeValue = nil
        }
        
        let matchNotifList:[structNotification] = DBChats.getUnreadNotifs(Const.NOTIF_MATCH_FOUND)
        let tabItem2:UITabBarItem = self.tabBar.items![0] 
        if matchNotifList.count > 0 {
            tabItem2.badgeValue = "\(matchNotifList.count)"
        }
        else {
            tabItem2.badgeValue = nil
        }
        
        if self.selectedIndex == 2 {
            let groups:inTuneVC = self.viewControllers![2] as! inTuneVC
            groups.loadData()
        }
        publicChatUpdates = true
    }

    override func viewDidDisappear(animated: Bool)
    {
       appDelegate.mainTabBar = nil
    }

    
    override func didReceiveMemoryWarning()
        
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIImage
{
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRectMake(0, 0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}
