
import UIKit

class showListCell: UITableViewCell
{
    @IBOutlet weak var displayLabel: UILabel!
    @IBOutlet var cellContainer: UIView!
    
    var concertStrct:structConcertDetails!
    
    func loadData(showStrct:structConcertDetails)
    {
        cellContainer.layer.cornerRadius = 4.0
        self.layoutMargins = UIEdgeInsetsZero
        concertStrct = showStrct
        displayLabel.text = showStrct.sConcertVenue
    }
}
