
import UIKit

class songListCell: UITableViewCell {

    @IBOutlet var cellContainer: UIView!
    @IBOutlet var displayLabel: UILabel!
    @IBOutlet var artistName: UILabel!
    @IBOutlet var songCheckArrowImg: UIImageView!
    
    var songStructure:structSongDetails!
    override func awakeFromNib() {
        super.awakeFromNib()
        cellContainer.layer.cornerRadius = 4.0
        self.layoutMargins = UIEdgeInsetsZero
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(songStrct:structSongDetails)
    {
        songStructure = songStrct
        displayLabel.text = songStructure.sSongsName
        artistName.text = songStructure.sSongArtist
        cellContainer.layer.borderWidth = 0
        cellContainer.layer.borderColor = UIColor(netHex: 0xCA1B31).CGColor
    }

}
