//
//  firstTutContentVC.swift
//  in-tune
//
//  Created by Anand Altekar on 09/09/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class firstTutContentVC: UIViewController
{
    @IBOutlet weak var imageView: UIImageView!
    
    var pageIndex:Int!
    var imageFile:String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.imageView.image = UIImage(named: self.imageFile)
        Utility.pageGredient(self.view)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
