//
//  FDViewController.swift
//  in-tune
//
//  Created by Anand Altekar on 27/07/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class FDViewController: UIViewController, _IFaceCallTarget
{
    
    
    @IBOutlet weak var feedbackText: UITextView!
    @IBOutlet var boxContainer: UIView!
    @IBOutlet var imageScale: UIImageView!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var matchStatusLbl: UILabel!
    let screenSize: CGFloat = UIScreen.mainScreen().bounds.width
    var maxViewLength:Int = 3
    var verticalDiff:CGFloat = 100
    var topDiff:CGFloat = 0
    var notifSoapObject:GetUpdatedNotifications!
    var matchNotifList:[structNotification] = Array<structNotification>()
    var mainTabBar:mainTabBarController!
    
    @IBOutlet var matchContainer: UIView!
    
    @IBAction func sendFeedback(sender: AnyObject)
    {
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        boxContainer.layer.cornerRadius = 9.0
        feedbackText.layer.cornerRadius = 5.0
        sendBtn.layer.cornerRadius = 3.0
        sendBtn.layer.borderWidth = 1
        sendBtn.layer.borderColor = UIColor.whiteColor().CGColor
        mainTabBar = self.tabBarController as! mainTabBarController
    }
    
    override func viewWillAppear(animated: Bool) {
        matchStatusLbl.hidden = true
        matchNotifList = DBChats.getUnreadNotifs(Const.NOTIF_MATCH_FOUND)
        if matchNotifList.count <= 0 {
            getNotifsFromServer()
        }
        else {
            genMatchViews()
        }
    }
    
    func getNotifsFromServer(){
        matchStatusLbl.text = "Checking for updates..."
        matchStatusLbl.hidden = false
        notifSoapObject = GetUpdatedNotifications()
        notifSoapObject.pageTarget = self
        notifSoapObject.request([publicSuidSession])
        let connector2:WSSoapConnector = WSSoapConnector()
        connector2.callAPI(notifSoapObject)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func genMatchViews(){
        var count:Int = matchNotifList.count
        if count > maxViewLength {
            count = maxViewLength
        }
        for view in self.matchContainer.subviews {
            view.removeFromSuperview()
        }
        for var s:Int=0; s<count; s++ {
            let index:Int = count - (s+1)
            let marginGap:CGFloat = CGFloat(screenSize*0.033)
            let leftMargin:CGFloat = marginGap*CGFloat(index+1)
            let indexWiseTopMargin:CGFloat = ((marginGap*2)+marginGap/2)*CGFloat(index+1)
            let widthVal:CGFloat = screenSize - ((marginGap*2)*CGFloat(index+1))
            let heightVal:CGFloat = widthVal + verticalDiff
            let matchScr:matchFoundView = matchFoundView()
            matchScr.parentView = self
            matchScr.frame = CGRectMake(leftMargin, topDiff+indexWiseTopMargin, widthVal, heightVal)
            matchScr.strGroup = Utility.notificationToGroup(matchNotifList[0])
            matchScr.message += "'\(matchNotifList[0].message)'"
            let userInfo:String = matchNotifList[0].other
            let userInfoArr:[String] = userInfo.componentsSeparatedByString(Const.NOTIF_JOIN_LVL2)
            for item in userInfoArr {
                var userDetailsArr:[String] = item.componentsSeparatedByString(Const.NOTIF_JOIN_LVL3)
                if Utility.suidToInt(publicSuidUser) != Int(userDetailsArr[0])! {
                    matchScr.userNameTxt = userDetailsArr[1]
                    break
                }
            }
            matchScr.notificationID = matchNotifList[0].id
            matchNotifList.removeAtIndex(0)
            self.matchContainer.insertSubview(matchScr, atIndex: s)
            if s == count-1 {
                matchScr.showContent()
            }
        }
    }
    
    func checkForNewView(){
        
    }
    
    func changePositions(newFrame:CGRect){
        var frame:CGRect = newFrame
        UIView.animateWithDuration(0.4, animations: {
            for var i:Int = self.matchContainer.subviews.count-1; i>=0; i-- {
                let uiVw:matchFoundView = self.matchContainer.subviews[i] as! matchFoundView
                let oldFrame:CGRect = uiVw.frame
                uiVw.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height)
                frame = oldFrame
            }
            
            }, completion: { finished in
                if self.matchContainer.subviews.count > 0 {
                    let uiVw:matchFoundView = self.matchContainer.subviews[self.matchContainer.subviews.count-1] as! matchFoundView
                    uiVw.showContent()
                    if self.matchNotifList.count > 0 {
                        let matchScr:matchFoundView = matchFoundView()
                        matchScr.parentView = self
                        matchScr.frame = frame
                        matchScr.strGroup = Utility.notificationToGroup(self.matchNotifList[0])
                        matchScr.message += "'\(self.matchNotifList[0].message)'"
                        matchScr.notificationID = self.matchNotifList[0].id
                        self.matchNotifList.removeAtIndex(0)
                        self.matchContainer.insertSubview(matchScr, atIndex: 0)
                    }
                }
        })
        
        if self.matchContainer.subviews.count == 1 {
            self.getNotifsFromServer()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        self.mainTabBar.addBadge()
        if target.methodName == "GetUpdatedNotifications" {
            self.matchStatusLbl.hidden = true
            self.matchNotifList = DBChats.getUnreadNotifs(Const.NOTIF_MATCH_FOUND)
            self.genMatchViews()
            if self.matchNotifList.count <= 0 {
                self.matchStatusLbl.text = "No match found."
                self.matchStatusLbl.hidden = false
            }
        }
    }
}
