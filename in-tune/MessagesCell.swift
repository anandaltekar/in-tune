
import UIKit

class MessagesCell: UITableViewCell
{

    @IBOutlet var cellBadge: UILabel!
    @IBOutlet var cellContainer: UIView!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var groupName: UILabel!
    @IBOutlet var groupContent: UILabel!
    @IBOutlet var multiImgContainer: UIView!
    
    @IBOutlet var imgFirst: UIImageView!
    @IBOutlet var imgSecond: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        cellContainer.layer.cornerRadius = 4.0
        self.layoutMargins = UIEdgeInsetsZero
//        imgFirst.layer.cornerRadius = imgFirst.bounds.height/8
//        imgSecond.layer.cornerRadius = imgSecond.bounds.height/8
        userImage.layer.cornerRadius = userImage.bounds.height/2
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bindData(groupDetails: structChatMsgGroup)
    {
        if groupDetails.userList.count>1
        {
            Utility.downloadFBPP(groupDetails.userList[0].fbProfUrl, imgView: imgFirst, isLargePic: false)
            Utility.downloadFBPP(groupDetails.userList[1].fbProfUrl, imgView: imgSecond, isLargePic: false)
            
            multiImgContainer.hidden = false
            userImage.hidden = true
            
        }
        else {
            Utility.downloadFBPP(groupDetails.userList[0].fbProfUrl, imgView: userImage, isLargePic: false)
            multiImgContainer.hidden = true
            userImage.hidden = false
        }
        cellBadge.text = String(groupDetails.newMsgFlag)
        groupName.text = Utility.userNamesExceptYou(groupDetails.userList)
        groupContent.text = groupDetails.name
        if groupDetails.newMsgFlag > 0 {
            setUnsetBadge(true)
        }
        else
        {
            setUnsetBadge(false)
        }
    }
    
    func setUnsetBadge(val:Bool)
    {
        if val
        {
            cellBadge.layer.cornerRadius = cellBadge.frame.height/2
            cellBadge.hidden = false
            cellContainer.layer.borderWidth = 2
            cellContainer.layer.borderColor = UIColor(netHex: 0xCA1B31).CGColor
        }
        else
        {
            cellBadge.hidden = true
            cellContainer.layer.borderWidth = 0
            cellContainer.layer.borderColor = UIColor(netHex: 0xCA1B31).CGColor
        }
    }
    
}
