

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SettingPage: UIViewController, _IFaceCallTarget {
    
    @IBOutlet var feedbackInput: UITextView!
    @IBOutlet var sendFeedbackBtn: UIButton!
    @IBOutlet var notificationSoundBtn: UISwitch!
    @IBOutlet var logoutBtn: UIButton!
    var soapObject: SendFeedback!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackInput.layer.cornerRadius = 5.0
        sendFeedbackBtn.layer.cornerRadius = 5.0
        logoutBtn.layer.cornerRadius = 8.0
        logoutBtn.layer.borderWidth = 1
        logoutBtn.layer.borderColor = UIColor.whiteColor().CGColor
        notificationSoundBtn.onTintColor = UIColor(netHex: 0xFF6300)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    @IBAction func sendFeedbackBtnAction(sender: UIButton) {
        dismissKeyboard()
        self.soapObject = SendFeedback()
        self.soapObject.request([publicSuidSession, feedbackInput.text])
        self.soapObject.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(self.soapObject)
    }
    
    @IBAction func notifSoundBtnAction(sender: AnyObject) {
        
    }
    
    @IBAction func logoutBtnAction(sender: AnyObject) {
        if SRTimer.timerStatus == true
        {
            //
        }
        else
        {
            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to log out?", preferredStyle: .ActionSheet)
            
            let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
                let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                fbLoginManager.logOut()
                Utility.resetPublicVars()
                self.navigationController?.popToRootViewControllerAnimated(false)
                self.appDelegate.chatRoom = nil
                self.appDelegate.mainTabBar = nil
            })
            let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alert.addAction(confirmAction)
            alert.addAction(CancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    func serviceResponse(target: _IFaceAPI)
    {
            if soapObject.respCode == 0
            {
                let alert = UIAlertController(title: "Thank You!", message: "Thank you for sending us your feedback", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                feedbackInput.text = ""
            }
            else
            {
                let alert = UIAlertController(title: "Oops!", message: "There was an error! please try again.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
    }
    
}
