//
//  entryCodeVC.swift
//  in-tune
//
//  Created by Anand Altekar on 12/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class entryCodeVC: UIViewController, _IFaceCallTarget
{
    @IBOutlet weak var codeCheck: UITextField!
    var codeSoapObject:VerifyConcertCode!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func codeConfirm(sender: AnyObject)
    {
        if codeCheck.text != ""
        {
            let params:[String] = [publicSuidSession, codeCheck.text!]
            codeSoapObject = VerifyConcertCode()
            codeSoapObject.pageTarget = self
            codeSoapObject.request(params)
            let soapConnector:WSSoapConnector = WSSoapConnector()
            soapConnector.callAPI(codeSoapObject)
        }
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if codeSoapObject.respCode == 0
        {
            self.performSegueWithIdentifier("codeToMainScreen", sender: self)
        }
        else
        {
            let alert = UIAlertController(title: "Oops!", message: "Are you really at (Venuename)?? please enter the code again!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
