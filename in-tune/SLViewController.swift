
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SLViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,  _IFaceCallTarget
{
    @IBOutlet var tableView: UITableView!
    var soapObject: GetConcertDetails!
    var soapSongUpdates:GetKilledSongs!
    var songSoapObject: SelectSongToPlay!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var timerLabelView: UIView!
    @IBOutlet var timerLabelContainer: UIView!
    @IBOutlet var viewTopMargin: NSLayoutConstraint!
    @IBOutlet var tblTopMargin: NSLayoutConstraint!
    
    @IBOutlet var tblLeftMargin: NSLayoutConstraint!
    @IBOutlet var tblBottomMargin: NSLayoutConstraint!
    @IBOutlet var tblRightMargin: NSLayoutConstraint!
    
    @IBOutlet var pageStatusLbl: UILabel!
    
    
    @IBOutlet weak var songRequestTimerLabel: UILabel!
    var refreshControl:UIRefreshControl!
    var SongList:[structSongDetails] = Array<structSongDetails>()
    var selectedIndexPath:NSIndexPath!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        timerLabelView.hidden = true
        Utility.pageGredient(self.view)
        timerLabelContainer.layer.cornerRadius = 4.0
        
        //Get song list
        SongList = DBChats.getSongList(Utility.suidToInt(selectedConcert))
        if SongList.count == 0
        {
            self.pageStatusLbl.hidden = false
            self.pageStatusLbl.text = "Checking for Song List ..."
            loadInitialData()
        }
        
        //Pull to refresh
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Reloading...")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        
        // done frame resize due to content overalapping on navigation error.
        let topMargin:CGFloat = self.navigationController!.navigationBar.frame.height + UIApplication.sharedApplication().statusBarFrame.height
        UIView.animateWithDuration(0.0, animations: {()-> Void in
            self.tblTopMargin.constant = topMargin
            self.viewTopMargin.constant = topMargin
            self.tblLeftMargin.constant = 16
            self.tblBottomMargin.constant = topMargin
            self.tblRightMargin.constant = 16
            self.view.layoutIfNeeded()
        })
        
        //tableview delegates assigning
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //check if timer is still running
        self.checkTimer()
    }

    override func viewDidAppear(animated: Bool)
    {
        self.checkTimer()
        // done frame resize due to content overalapping on navigation error.
        UIView.animateWithDuration(0, animations: {()-> Void in
            self.tblTopMargin.constant = 0
            self.viewTopMargin.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func refresh()
    {
        callToGetConcertDetails()
    }
    
    func backBtnAction()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func checkTimer()
    {
        if SRTimer.timerStatus
        {
            SRTimer.parent = self
            songRequestTimerLabel.hidden = false
            self.tableView.userInteractionEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SongList.count
        //self.soapObject.ConcertDetails.SongList.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("songListItem") as! songListCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.accessoryType = .None
        if self.SongList[indexPath.row].nRequestedStatus == Const.ALREADY_REQUESTED
        {
            cell.songCheckArrowImg.image = UIImage(named: "selected_song.png")

        }
        else
        {
            cell.songCheckArrowImg.image = UIImage(named: "next_arrow.png")
            cell.userInteractionEnabled = true

        }
        cell.loadData(self.SongList[indexPath.row])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? songListCell
        {
            selectedIndexPath = indexPath
            if self.SongList[indexPath.row].nRequestedStatus == Const.ALREADY_REQUESTED
            {
//                let alertView = UIAlertController(title: "Repeated Request!", message: "You have already requested this song, are you sure you want to ask for it again?", preferredStyle: .Alert)
//                alertView.addAction(UIAlertAction(title: "Yes!", style: .Default, handler: { (alertAction) -> Void in
//                    self.timerStart()
//                    self.callToSelectSongToPlay(indexPath)
//    
//                }))
//                alertView.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                let alertController = UIAlertController(title: "Repeated Request!", message: "You have already requested this song.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: {Void in alertController.dismissViewControllerAnimated(false, completion: nil)}
                    ))
                
                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
               // presentViewController(alertView, animated: true, completion: nil)
            }
            else
            {
                self.timerStart()
                self.SongList[indexPath.row].nRequestedStatus = Const.ALREADY_REQUESTED
                DBChats.updateSong(self.SongList[indexPath.row].id, updateField: Const.DBFIELD_REQSTD_STAT, value: Const.ALREADY_REQUESTED)
                cell.songCheckArrowImg.image = UIImage(named: "selected_song.png")
                self.callToSelectSongToPlay(indexPath)
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func callToSelectSongToPlay(indexPath:NSIndexPath)
    {
        let params:[String] = [publicSuidSession, Utility.intToSuid(self.SongList[indexPath.row].id)]
        songSoapObject = SelectSongToPlay()
        songSoapObject.request(params)
        songSoapObject.pageTarget = self
        let soapConnector: WSSoapConnector = WSSoapConnector()
        soapConnector.callAPI(songSoapObject)

    }
    
    func callToGetConcertDetails()
    {
        soapSongUpdates = GetKilledSongs()
        soapSongUpdates.request([publicSuidSession, selectedConcert])
        soapSongUpdates.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(soapSongUpdates)
    }
    
    func loadInitialData()
    {
        soapObject = GetConcertDetails()
        soapObject.request([publicSuidSession, selectedConcert])
        soapObject.pageTarget = self
        let connector: WSSoapConnector = WSSoapConnector()
        connector.callAPI(soapObject)
    }
    
    func SLTV()
    {
        self.tableView.userInteractionEnabled = true
    }
    
    func timerStart(){
        let startTime = NSDate.timeIntervalSinceReferenceDate()
        let startTimeInt: Int = Int(startTime)
        userDefaults.setValue(startTimeInt, forKey: "startTimeUse") //storing startTime locally
        SRTimer.parent = self
        SRTimer.startSongRequestTimer()
    }
    
    func serviceResponse(target: _IFaceAPI)
    {
        if target.methodName == "SelectSongToPlay"
        {
            if songSoapObject.respCode != 0
            {
                //display alert with error
                SRTimer.stop()
                self.tableView.userInteractionEnabled = true
                self.SongList[selectedIndexPath.row].nRequestedStatus = Const.NOT_REQUESTED
                DBChats.updateSong(self.SongList[selectedIndexPath.row].id, updateField: Const.DBFIELD_REQSTD_STAT, value: Const.NOT_REQUESTED)
                let cell:songListCell =  self.tableView.cellForRowAtIndexPath(selectedIndexPath) as! songListCell
                cell.songCheckArrowImg.image = UIImage(named: "next_arrow.png")
            }
        }
        else
        {
            SongList = DBChats.getSongList(Utility.suidToInt(selectedConcert))
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
        
        if self.SongList.count <= 0 {
            self.pageStatusLbl.hidden = false
            self.pageStatusLbl.text = "Songs not Available!"
        }
        else {
            self.pageStatusLbl.hidden = true
        }
    }
}
