//
//  Timers.swift
//  in-tune
//
//  Created by Anand Altekar on 04/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import Foundation
//import UIKit
var timerDiff:Int = 0
var sleepTime:Int = 0
var wakeTime:Int = 0
class Timers: NSObject
{
    var timer = NSTimer()
    var SRTTotalTime                = 30    //Song Request Timer total time
//    var SMTTotalTime                = 300    //Single Match Timer total time
//    var GrMTTotalTime               = 300    //Group Match Timer total time
//    var GTTotalTime                 = 600    //Global Timer total time
    var SRTcounter: Int             = Int()    //counter for Song Request
    var timerStatus:Bool            = false
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var parent:SLViewController!
    
    func startSongRequestTimer()
    {
        timerStatus = true
        let startTimeUse:Int = userDefaults.valueForKey("startTimeUse") as! Int
        let currentTime: NSTimeInterval = NSDate.timeIntervalSinceReferenceDate()
        let currentTimeUse:Int = Int(currentTime)
        SRTcounter = SRTTotalTime - (currentTimeUse - startTimeUse)
        if SRTcounter > 0
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateSRTimer"), userInfo: nil, repeats: true)
            timer.fire()
        }
        else
        {
            userDefaults.removeObjectForKey("startTimeUse")
            timerStatus = false
        }
    }
    func updateSRTimer()
    {
        timerDiff = wakeTime - sleepTime
        if timerDiff > 0
        {
            SRTcounter =  SRTcounter - timerDiff
            timerDiff = 0
            sleepTime = 0
            wakeTime = 0
        }
        else
        {
        SRTcounter--
        }
        if SRTcounter < 1
        {
            stop()
        }
        else
        {
            let minutes = UInt8 (SRTcounter / 60)
            let seconds = UInt8 (SRTcounter % 60)
            let strMinutes = String(format: "%02d", minutes)
            let strSeconds = String(format: "%02d", seconds)
            if parent != nil
            {
                parent.timerLabelView.hidden = false
                parent.songRequestTimerLabel.text = "\(strMinutes):\(strSeconds)"
                parent.tableView.userInteractionEnabled = false
            }
        }
    }
    
    func stop()
    {
        timer.invalidate()
        timerStatus = false
        userDefaults.removeObjectForKey("startTimeUse")
        if parent != nil
        {
            parent.timerLabelView.hidden = true
            parent.tableView.userInteractionEnabled = true
        }
    }
    
    override init()
    {
        super.init()
    }
}

var SRTimer: Timers = Timers()
