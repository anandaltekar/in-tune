//
//  chatGroupDetailsVC.swift
//  in-tune
//
//  Created by Anand Altekar on 31/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class chatGroupDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var userInfo: [structChatMsgUser]!
    var groupID: Int!
    
    override func viewDidLoad()
    { 
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        userInfo = DBChats.getGroupUsers(groupID)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        
        let rightBtn : UIBarButtonItem = UIBarButtonItem(title: "Delete", style: UIBarButtonItemStyle.Plain, target: self, action: "DeleteAction")
        rightBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = rightBtn
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Arial", size: 18)!], forState: UIControlState.Normal)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    // MARK: - Table view data source
    
    func DeleteAction()
    {
        let alert = UIAlertController(title: "Delete Group", message: "Are you sure you want to leave this group?", preferredStyle: .ActionSheet)
        
        let confirmAction = UIAlertAction(title: "Confrim", style: UIAlertActionStyle.Destructive, handler: {(Bool) in
            self.navigationController?.popToViewController(self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 3] , animated: true)
            DBChats.updateGroupStatus(self.groupID, value: Const.INACTIVE)
            
            let soapObject:RemoveUserFromCommuniqueBox = RemoveUserFromCommuniqueBox()
            soapObject.request([publicSuidSession, Utility.intToSuid(self.groupID)])
            let soapConnector:WSSoapConnector = WSSoapConnector()
            soapConnector.callAPI(soapObject)
        })
        let CancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(CancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return userInfo.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("CGDItem") as! CGDCell
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.userInfo = userInfo[indexPath.row]
        cell.bindData()
        
        return cell
    }
}
