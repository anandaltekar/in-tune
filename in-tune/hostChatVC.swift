//
//  hostChatVC.swift
//  in-tune
//
//  Created by Anand Altekar on 25/08/15.
//  Copyright (c) 2015 FDS Infotech Pvt Ltd. All rights reserved.
//

import UIKit

class hostChatVC: UIViewController, UITableViewDataSource, UITableViewDelegate, _IFaceCallTarget
{
    
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var sendChatBtn: UIButton!
    @IBOutlet weak var chatInputField: UITextField!
    @IBOutlet var bottomMargin: NSLayoutConstraint!
    
    var soapObject:GetCommuniqueBoxNotes!
    var soapObjSendMsg:SendNoteToCommuniqueBox!
    var idGroup:Int = 0
    var marginVal:CGFloat = 0

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Utility.pageGredient(self.view)
        tableVw.delegate = self
        tableVw.dataSource = self
        let nib = UINib(nibName: "chatRoomMsgOther", bundle: nil)
        tableVw.registerNib(nib, forCellReuseIdentifier: "cell")
        frameView.layer.cornerRadius = 4.0
        tableVw.estimatedRowHeight = 20.0
        tableVw.rowHeight = UITableViewAutomaticDimension
        self.tabBarController?.tabBar.hidden = true
        sendChatBtn.layer.cornerRadius = 5.0
        let tapDismissKeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapDismissKeyboard)
        Utility.pageGredient(self.view)
        
        let rightBtn : UIBarButtonItem = UIBarButtonItem(title: "Details", style: UIBarButtonItemStyle.Plain, target: self, action: "DetailsAction")
        rightBtn.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = rightBtn
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Arial", size: 18)!], forState: UIControlState.Normal)

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    func keyboardWillShow(sender: NSNotification) {
        let frame = (sender.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.marginVal = frame.height
        resizeWithKeyboard()
    }
    
    func keyboardWillHide(sender: NSNotification) {
       // let frame = (sender.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.marginVal = 0
        resizeWithKeyboard()
    }
    
    func resizeWithKeyboard()
    {
        UIView.animateWithDuration(0.35, animations: {()-> Void in
            self.bottomMargin.constant = self.marginVal
            self.view.layoutIfNeeded()
        })
    }
    
    func DetailsAction()
    {
        performSegueWithIdentifier("showHostChatDetails", sender: self)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        soapObject = GetCommuniqueBoxNotes();
        soapObject.pageTarget = self
        let params:[String] = [publicSuidSession, Utility.intToSuid(idGroup)]
        soapObject.request(params)
        let serviceConnector:WSSoapConnector = WSSoapConnector()
        serviceConnector.callAPI(soapObject)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    @IBAction func sendHostChat(sender: AnyObject)
    {
        if chatInputField.text != ""
        {
            let params:[String] = [publicSuidSession, Utility.intToSuid(idGroup), chatInputField.text!]
            soapObjSendMsg = SendNoteToCommuniqueBox()
            soapObjSendMsg.request(params)
            let connector:WSSoapConnector = WSSoapConnector()
            connector.callAPI(soapObjSendMsg)
            var chatMsg:structChatMsg = structChatMsg()
            chatMsg.text = chatInputField.text!
            chatMsg.type = Const.OWN_MSG
            chatMsg.user.name = "Me"
            chatMsg.user.id = Utility.suidToInt(publicSuidUser)
            soapObject.chatList.append(chatMsg)
            chatInputField.text = ""
            self.tableVw.reloadData()
        }

    }
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    //Mark:- Table View Data Source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return soapObject.chatList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let row:Int = indexPath.row as Int
        let cell:chatRoomMsg = tableView.dequeueReusableCellWithIdentifier("cell") as! chatRoomMsg
        cell.backgroundColor = UIColor.clearColor()
        cell.createMsg(soapObject.chatList[row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "showHostChatDetails"
        {
            if let destinationVC = segue.destinationViewController as? hostChatDetailsVC
            {
                destinationVC.groupID = self.idGroup
            }
        }
    }

    func serviceResponse(target: _IFaceAPI)
    {
        self.tableVw.reloadData()
    }
}
